# demo/settings.py
INSTALLED_APPS = (
  'django.contrib.auth',
  'django.contrib.sessions',
  'django.contrib.sites',
  'django.contrib.admin',
  'schedule',
  ...
)
